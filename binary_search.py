# [3, 6.1, 6.1, 9, 9, 9, 9, 9, 15.6, 15.6, ...1230981230981320]
#
# We have a large vector of sorted integers with dublicates
# find an algorithm to efficiently count how many instances of a specific number are in the vector

# 9 -> 5

def binary_search(list, number):
  min = 0
  max = len(list) - 1
  while True:
    if max < min:
      return -1
    center = (max + min) / 2
    if list[center] < number:
      min = center + 1
    elif list[center] > number:
      max = center - 1
    else:
      return center
    
search_number = 9    

a = [3, 6.1, 6.1, 9, 9, 9, 9, 9, 15.6, 15.6]

ind = binary_search(a, search_number)

if ind:
  count = 1

  for i in xrange(ind-1, 0, -1):
    if a[i] == search_number:
        count += 1
    else:
        break

  for i in xrange(ind+1, len(a)):
    if a[i] == search_number:
        count += 1
    else:
        break

print count

    