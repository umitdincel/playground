# Source links
# https://www.quora.com/How-is-the-fuzzy-search-algorithm-in-Sublime-Text-designed
# http://jsfiddle.net/bulat/CCryL/
# Thanks ;)

import requests
universities = requests.get('https://raw.githubusercontent.com/Hipo/university-domains-list/master/world_universities_and_domains.json').json()
university_list = []

# search for big list
for university in universities:
  university_list.append(university['name'].lower())

zen_of_python = [
  "Beautiful is better than ugly.",
  "Explicit is better than implicit.",
  "Simple is better than complex.",
  "Complex is better than complicated.",
  "Flat is better than nested.",
  "Sparse is better than dense.",
  "Readability counts.",
  "Special cases aren't special enough to break the rules.",
  "Although practicality beats purity.",
  "Errors should never pass silently.",
  "Unless explicitly silenced.",
  "In the face of ambiguity, refuse the temptation to guess.",
  "There should be one-- and preferably only one --obvious way to do it.",
  "Although that way may not be obvious at first unless you're Dutch.",
  "Now is better than never.",
  "Although never is often better than *right* now.",
  "If the implementation is hard to explain, it's a bad idea.",
  "If the implementation is easy to explain, it may be a good idea.",
  "Namespaces are one honking great idea -- let's do more of those!",
]

def fuzzy_search(search_set, text):
    for i, word in enumerate(text.split(' ')):
        token = list(word.lower())
        
        if i is not 0:
          search_set = matches
        
        matches = []
        for string in search_set:
          token_index, string_index = 0, 0
          matched_positions = []
          string = string.lower()

          while string_index < len(string):
            if string[string_index] == token[token_index]:
              matched_positions.append(string_index)
              token_index += 1

              if token_index >= len(token):
                # matches.append({
                #     'match': string,
                #     'positions': matched_positions
                # })
                matches.append(string)

                break
            string_index += 1

    return matches

print fuzzy_search(zen_of_python, 'thn dnse')
print fuzzy_search(university_list, 'blck')

# Some Responses for `zen_of_python`
# cmplx search, its found `simple is better than complex.`, `complex is better than complicated.`


