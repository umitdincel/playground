<?php
	// EDIT THE 2 LINES BELOW AS REQUIRED
	$email_to = "melike.erol@peninsula.com.tr";
	$email_subject = "Biz Sizi Arayalım Formu";


	function died_contact($error) {
			// your error code can go here
			echo "<div class='form-message false'><ul>";
				echo $error;
			echo "</ul></div>";
	}

	// validation expected data exists
	if(!isset($_POST['your-name']) ||
			!isset($_POST['page-url']) ||
			!isset($_POST['your-email']) ||
			!isset($_POST['your-telephone'])) {
			died_contact('We are sorry, but there appears to be a problem with the form you submitted.');
	}

	$page_url = $_POST['page-url']; // required
	$form_name = $_POST['your-name']; // required
	$form_email = $_POST['your-email']; // required
	$form_phone = $_POST['your-telephone']; // required

	$error_message = "";

	if(strlen($form_name) < 2 ) {
		$error_message .= '<li>Lütfen adınızı ve soyadınızı yazınız.</li>';
	}

	$email_exp = '/^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/';
	if(!preg_match($email_exp,$form_email)) {
		$error_message .= "<li>Lütfen geçerli bir e-posta adresi yazınız.</li>";
	}

	if(strlen($form_phone) < 2) {
		$error_message .= '<li>Lütfen telefon numaranızı yazınız.</li>';
	}


	if(strlen($error_message) > 0) {
		died_contact($error_message);
	}
	else
	{
		$email_message = "Mesaj detayı:\n\n";

		function clean_string($string) {
			$bad = array("content-type","bcc:","to:","cc:","href");
			return str_replace($bad,"",$string);
		}

		$email_message .= "Adı ve Soyadı: ".clean_string($form_name)."\n\n";
		$email_message .= "E-Posta: ".clean_string($form_email)."\n\n";
		$email_message .= "Telefon: ".clean_string($form_phone)."\n\n";
		$email_message .= "Mesaj: ".clean_string($page_url)."\n\n";
		$email_message .= "Konu: Biz Sizi Arayalım Formu";


		// create email headers
		$email_message = trim(stripslashes($email_message));
		@$send=mail($email_to, $email_subject, $email_message, "From: \"$form_name\" <".$form_email.">\nReply-To: \"".ucwords($form_name)."\" <".$form_email.">\nX-Mailer: PHP/" . phpversion() );
		if ($send) { echo "<div class='form-message true'>Mesajınız İletildi.</div>";}
		else { echo "<div class='form-message false'>Mesajınız İletilmedi.</div>"; }
	}
?>

<!-- include your own success html here -->
